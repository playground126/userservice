package com.example.userservice.entity;

import lombok.Builder;
import lombok.Data;

/**
 * ユーザーエンティティ
 */
@Data
@Builder
public class User {

    private long id;

    private String mail;

    private String name;
}

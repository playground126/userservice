package com.example.userservice.controller;

import com.example.userservice.entity.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * ユーザーのコントローラー
 */
@RestController
public class UserController {

    /**
     * ログイン処理
     * @return
     */
    @GetMapping("/")
    public ResponseEntity<String> login() {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body("Hello");
    }

    @GetMapping("/user/{mail}")
    public ResponseEntity<String> getUser(@PathVariable String mail) {
        ObjectMapper mapper = new ObjectMapper();

        // TODO サービス経由でUserを取得する
        User res = User.builder().id(1).mail(mail).name("田中太郎").build();

        try {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(mapper.writeValueAsString(res));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND).body("ユーザーの取得に失敗");
    }
}
